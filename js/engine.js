$(function() {

$('.calendar-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false
});

// FansyBox 2

$('.fancybox').fancybox();


 // BEGIN of script for header submenu
$(".navbar-toggle").on("click", function () {
    $(this).toggleClass("active");
});


// Календарь бутстрап

$('#datetimepicker1').datetimepicker({
    locale: 'ru',
    inline: true,
    viewMode: 'days'
        
});

$('#datetimepicker2').datetimepicker({
    locale: 'ru',
    inline: true,
    viewMode: 'days'
        
});

$('#datetimepicker3').datetimepicker({
    locale: 'ru',
    inline: true,
    viewMode: 'days'
        
});

$('#datetimepicker4').datetimepicker({
    locale: 'ru',
    inline: true,
    viewMode: 'days'
        
});



// Позиционирование блока над картой
var windowWidth = $( window ).width();
var containerWidth = $( ".container" ).width();
$('.contacts-box').css("left", (windowWidth-containerWidth )/2);




//Begin of GoogleMaps
  var myCenter=new google.maps.LatLng(55.773917, 37.694717);
  var marker;
  function initialize()
  {
    var mapProp = {
    center:myCenter,
    zoom:16,
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
    },
    mapTypeId:google.maps.MapTypeId.ROADMAP
    };
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
      var marker=new google.maps.Marker({
      position:myCenter,
      });
    marker.setMap(map);
    var content = document.createElement('div');
    content.innerHTML = '<strong class="maps-caption">Rennicks (UK) Ltd</strong>';
    var infowindow = new google.maps.InfoWindow({
     content: content
    });
      google.maps.event.addListener(marker,'click',function() {
        infowindow.open(map, marker);
        map.setCenter(marker.getPosition());
      });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
//End of GoogleMaps




})